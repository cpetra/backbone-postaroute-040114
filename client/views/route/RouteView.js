define([
    'jquery',
    'underscore',
    'backbone',
    'config',
    'text!client/templates/route/RouteTemplate.html',
    'client/models/route/RouteViewModel'

    ], function ($, _, Backbone, Config, RouteTemplate, RouteViewModel) {

        var RouteView = Backbone.View.extend({

            // cache the template function for a single item.
            template : _.template(RouteTemplate),
            model : RouteViewModel,
            tagName : 'div',

            defaults:{

            },

            events:{
               
            },

            initialize: function (options) {
                this.domElem = options.domElem;
                this.render();

                
            },

            //draw the page by adding the template to the dom element el
            render : function () {
                this.$el.html(this.template());
                this.domElem.html(this.el);
                this.addEvents();
                this.createMap();
               
            },

            addEvents: function() {
                console.log('addEvents');
                $("#datepicker").datepicker();
            },

            createMap: function() {
                var mapCanvas = $('#map-canvas')[0];
                var mapOptions = {
                  center: new google.maps.LatLng(44.5403, -78.5463),
                  zoom: 8,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                var map = new google.maps.Map(mapCanvas, mapOptions);

                
                var input = $('#startLocation')[0];
              //  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);


                var inputEnd = $('#endLocation')[0];
                var autocompleteEnd = new google.maps.places.Autocomplete(inputEnd);
                autocompleteEnd.bindTo('bounds', map);

                google.maps.event.addListener(autocomplete, 'place_changed', function() {
                    console.log('place_changed');
                });

            }

        });


        return RouteView;
    }
);

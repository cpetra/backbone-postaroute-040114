define([
    'jquery',
    'underscore',
    'backbone',
    'config',
    'text!client/templates/post/PostTemplate.html',
    'client/models/post/PostViewModel'

    ], function ($, _, Backbone, Config, PostTemplate, PostViewModel) {

        var PageView = Backbone.View.extend({

            // cache the template function for a single item.
            template : _.template(PostTemplate),
            model : PostViewModel,
            tagName : 'div',

            defaults:{

            },

            events:{
                'click .post-button': 'onClickPost'
            },

            initialize: function (options) {
                this.domElem = options.domElem;
                //instantiate our model
                this.model = new PostViewModel();

                //do the client side validation
                this.model.on("invalid", this.onModelClientError, this);

                //do the server side validation
                this.model.on("error", this.onModelServerError, this);

                //when server response it's success do something
                this.model.on("sync", this.onModelSync, this);

                $('.post-button').hide();
                console.log($('.post-button'));

                this.render();

            },

            //draw the page by adding the template to the dom element el
            render : function () {
                this.$el.html(this.template());
                this.domElem.html(this.el);
                this.addEvents();
               
            },

            addEvents: function() {
                console.log($('.post-button'));
                $('.post-button').on('click', this.onClickPost, this);
            },

            onModelSync: function(model, response, options) {
                console.log('model sync');
               
            },

            onModelServerError:function (model, xhr, options) {
                console.log("on model server error");
                var resp = $.parseJSON(xhr.responseText);

            },

            onModelClientError: function(model, errors) {
               console.log("model client error");
            },

            onClickPost: function() {
                console.log("click post");
            }
        });


        return PageView;
    }
);

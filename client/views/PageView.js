define([
    'jquery',
    'underscore',
    'backbone',
    'config',
    'text!client/templates/PageTemplate.html',
    'client/models/PageModel',
    'client/views/post/PostView',
    'client/views/vehicle/VehicleView',
    'client/views/route/RouteView'

    ], function ($, _, Backbone, Config, PageTemplate, PageModel, PostView, VehicleView, RouteView ) {

        var PageView = Backbone.View.extend({

            // cache the template function for a single item.
            template : _.template(PageTemplate),
            model : PageModel,
            className : 'client-container',
            tagName : 'div',

            defaults:{

            },

            events:{

            },

            initialize: function (options) {
                console.log('Page view');
                this.domElem = options.domElem;
                this.render();
            },

            //draw the page by adding the template to the dom element el
            render : function () {
                console.log('Page render');
                this.$el.html(this.template());
                this.domElem.html(this.el);

                this.postContainer = $(".post-container");
                this.vehicleContainer = $(".vehicle-container");
                this.routeContainer  = $(".route-container");
                this.createPages();
            },

            createPages: function() {
                 var postView = new PostView({domElem: this.postContainer});
                 var vehicleView = new VehicleView({domElem: this.vehicleContainer});
                 var routeView = new RouteView({domElem: this.routeContainer});
            }
        });


        return PageView;
    }
);

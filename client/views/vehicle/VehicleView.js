define([
    'jquery',
    'underscore',
    'backbone',
    'config',
    'text!client/templates/vehicle/VehicleTemplate.html',
    'client/models/vehicle/VehicleViewModel'

    ], function ($, _, Backbone, Config, VehicleTemplate, VehicleViewModel) {

        var VehicleView = Backbone.View.extend({

            // cache the template function for a single item.
            template : _.template(VehicleTemplate),
            model : VehicleViewModel,
            tagName : 'div',

            defaults:{

            },

            events:{
               
            },

            initialize: function (options) {
                this.domElem = options.domElem;
                this.render();
            },

            //draw the page by adding the template to the dom element el
            render : function () {
                this.$el.html(this.template());
                this.domElem.html(this.el);
                this.addEvents();
               
            },

            addEvents: function() {
                console.log('addEvents');
               
            },

            

           
        });


        return VehicleView;
    }
);

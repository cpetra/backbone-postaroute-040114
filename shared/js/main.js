require.config({

	paths:{

		jquery:'../../shared/js/libs/jquery/jquery-min',
		jquery_cookie:'../../shared/js/libs/jquery/jquery.cookie',
		underscore:'../../shared/js/libs/underscore/underscore-min',
		backbone:'../../shared/js/libs/backbone/backbone0.9.10-min',
		dotdotdot:'../../shared/js/libs/dotdotdot/dotdotdot',
		tweenmax:'../../shared/js/libs/greensock/TweenMax',
		ui:'../../shared/js/libs/jquery_ui/jquery-ui-1.8.20.custom.min',
		config:'../../shared/js/libs/config',
		utils:'../../shared/js/libs/utils',
		text:'../../shared/js/libs/require/text',
		bootstrap:'../../shared/js/libs/bootstrap/bootstrap.min',
		client: '../../client',
		admin: '../../admin',
		shared: '../../shared'

	},

	shim:{
		'jquery_cookie':{
			deps:['jquery'],
			exports:'cookie'
		},
		'underscore':{
			exports:'_'
		},
		'backbone':{
			deps:['underscore', 'jquery'],
			exports:'Backbone'
		},
		'dotdotdot': {
			deps:['jquery'],
			exports:'DotDotDot'
		},
		'tweenmax': {
			deps:['jquery'],
			exports:'TweenMax'
		},
		'ui': {
			deps:['jquery'],
			exports:'Ui'
		},

		'bootstrap': {
			deps:['jquery'],
			exports:'Bootstrap'
		},

		'config': {
			exports:'Config'
		}
	}
});

require([

	// Load our init module and pass it to our definition function
	'init'

], function (initModule) {

	// declare a global varibale that will be used to trigger events
	window.App = {
		Vent : _.extend({}, Backbone.Events)
	};

	initModule.initialize();

});


define(
	['jquery', 'underscore', 'backbone', 'config', 'ui'],

	//verify library loads
	function (jQueryLocal, underscoreLocal, backboneLocal, Config, Ui) {

	}
);

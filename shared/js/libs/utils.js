define([
    'jquery'
], function($) {

});

function htmlEncode(value){
  return $('<div/>').text(value).html();
}

$.fn.serializeObject = function() {
  var o = {};
  var a = this.serializeArray();
  $.each(a, function() {
      if (o[this.name] !== undefined) {
          if (!o[this.name].push) {
              o[this.name] = [o[this.name]];
          }
          o[this.name].push(this.value || '');
      } else {
          o[this.name] = this.value || '';
      }
  });
  return o;
};

$.ajaxPrefilter( function( options, originalOptions, jqXHR ) {
  options.url = 'http://citizen-dev.thedevelopmentfactory.com/api' + options.url;
});

function createCookie(name, value, sec) {
    if (sec) {
        var date = new Date();
        date.setTime(date.getTime() + (sec * 1000));
        var expires = "; expires=" + date.toGMTString();
        console.log( expires);
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}


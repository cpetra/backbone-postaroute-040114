// Filename: router.js
define([
	'jquery',
	'underscore',
	'backbone',
	'shared/js/SiteApp'
], function ($, _, backbone, SiteApp) {

	var initialize = function () {
		var siteApp = new SiteApp();
		var app_router = new AppRouter();
		App.router = app_router;
		Backbone.history.start({
			pushState:false,
			// hashChange: false,
			silent : false,
			root:'#'
		});
	};

	var AppRouter = Backbone.Router.extend({
		routes:{
			// Client routes
			'/':'pageAction',
			'':'pageAction',

			'*actions': 'defaultAction'
		},

		// catch all routes and redirect to client
		defaultAction:function (page, action, identifier) {

			if ( confirm('Page not found... redirect?') ) {
				this.navigate('#', {trigger:true} );
			}
		},

		//called when viewing the client app
		pageAction:function (page, action, identifier) {
			console.log('page is :' + page + ' action is :' + action + ' identifier is :' + identifier);
			App.Vent.trigger('init', {
				key : 'init',
				page: page,
				action : action,
				pageId : identifier
			});
		}

	});

	return {
		initialize:initialize
	};
});

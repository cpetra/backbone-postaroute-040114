define([
    'jquery',
    'underscore',
    'backbone',
    'config',
    'client/views/PageView',
    'client/views/post/PostView'
   

    ], function ($, _, Backbone, Config, PageView, PostView) {

        var SiteAppView = Backbone.View.extend({

            initialize: function () {
                this.siteContainer = $(".site-container");
                App.Vent.on('init', this.showClient, this);
            },

            showClient: function () {
                var pageView = new PageView({domElem: this.siteContainer});
            }
        });


        return SiteAppView;
    }
);

define([
	'jquery',
	'underscore',
	'backbone',
	'router'
], function ($, _, Backbone, router) {
	var initialize = function () {
		// Pass in our Router module and call it's initialize function
		router.initialize();
	};

	return {
		initialize:initialize
	};
});
